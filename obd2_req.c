/*
    obd2_req

    Copyright 2019 Miguel A. Pérez Valdenebro (miguelangel.perezvaldenebro@wilbord.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

/* 
 * Control de cambios
 *
 * 12/01/2021	mapv	Añadida la opción -h para visualizar la ayuda.
 * 			Se corrige la frecuencia de 1 Hz., que no funcionaba.
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <ctype.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>

#include <net/if.h>

#include <linux/can.h>
#include <linux/can/bcm.h>

void ayuda( void )
{
	printf("\nOpciones :\n\n");
	printf("-i INTERFAZ_CAN\n\tEspecifica el interfaz CAN por el que se establece la comunicación. Por defecto, se usa can0.\n\n");
	printf("-f FRECUENCIA\n\tEspecifica la frecuencia (en Hz) a la que se desean las  solicitudes. Por defecto se usa 10 Hz.\n\n");
	printf("-c CANID\n\tEspecifica el identidicador CAN del mensaje a enviar (en hexadecimal). Por defecto se usa 0x7DF.\n\n");
	printf("-s SERVICIO\n\tEspecifica el servicio OBDII (en hexadecimal) a utilizar. Por defecto se usa 0x01.\n\n");
	printf("-p PID\n\tEspecifica el PID (en hexadecimal) de servicio OBDII a utilizar. Por defecto se usa 0x03.\n\n");
	printf("-h\n\tMuestra esta ayuda.\n");
}


int main( int argc, char*argv[] )
{
        int rc;
        int sock_fd;

        struct sockaddr_can addr;
        struct ifreq ifr;

        struct {
                struct bcm_msg_head msg_head;
                struct can_frame frame[1];
        } msg;

	char	_can_servicio[4] = "01";
	char	_can_frecuencia[4] = "10";
	char	_can_interfaz[16] = "can0";
	char	_can_pid[3] = "03";
	char	_can_id[8] = "7DF";

	int 	can_frecuencia = 10;
	int 	can_pid = 0x03;
	int 	can_id = 0x7DF;
	int 	can_servicio = 0x01;

	int c;
	
	char *endptr;

	opterr = 0;

	while ( (c = getopt (argc, argv, "c:i:f:p:s:h") ) != -1)
    		switch (c)
      		{
      			case 'h':
        			ayuda();
				exit(EXIT_SUCCESS);

      			case 'c':
        			strcpy( _can_id, optarg );

				can_id = strtol( _can_id, &endptr, 16 ); /* Base 16 */

           			/* Validación de posibles errores */

           			if( errno != 0 && can_id == 0 )
				{
               				perror("strtol");
               				exit(EXIT_FAILURE);
           			}

           			if (endptr == _can_id)
				{
               				fprintf(stderr, "No se encontró valor para el CAN ID\n");
               				exit(EXIT_FAILURE);
           			}

        			break;

      			case 'i':
        			strcpy( _can_interfaz, optarg );
        			break;

      			case 'f':
        			strcpy( _can_frecuencia, optarg );

				can_frecuencia = strtol( _can_frecuencia, &endptr, 10 ); /* Base 10 */

           			/* Validación de posibles errores */

           			if( errno != 0 && can_frecuencia == 0 )
				{
               				perror("strtol");
               				exit(EXIT_FAILURE);
           			}

           			if( endptr == _can_frecuencia )
				{
               				fprintf(stderr, "No se encontró valor para la frecuencia\n");
               				exit(EXIT_FAILURE);
           			}

        			break;

      			case 'p':
        			strcpy( _can_pid, optarg );

				can_pid = strtol( _can_pid, &endptr, 16 ); /* Base 16 */

           			/* Validación de posibles errores */

           			if( errno != 0 && can_pid == 0 )
				{
               				perror("strtol");
               				exit(EXIT_FAILURE);
           			}

           			if (endptr == _can_pid)
				{
               				fprintf(stderr, "No se encontró valor para el PID\n");
               				exit(EXIT_FAILURE);
           			}

        			break;

      			case 's':
        			strcpy( _can_servicio, optarg );

				can_servicio = strtol( _can_servicio, &endptr, 16 ); /* Base 16 */

           			/* Validación de posibles errores */

           			if( errno != 0 && can_servicio == 0 )
				{
               				perror("strtol");
               				exit(EXIT_FAILURE);
           			}

           			if (endptr == _can_servicio)
				{
               				fprintf(stderr, "No se encontró valor para el servicio OBDII\n");
               				exit(EXIT_FAILURE);
           			}

        			break;

      			default:
        			ayuda();
				exit(EXIT_FAILURE);
      		}


	printf("INTERFAZ ....: %s\n", _can_interfaz );
	printf("FRECUENCIA ..: %s - %d\n", _can_frecuencia, can_frecuencia );
	printf("CAN ID ......: 0x%s - %d\n", _can_id, can_id );
	printf("CAN SERVICIO : 0x%s - %d\n", _can_servicio, can_servicio );
	printf("CAN PID .....: 0x%s - %d\n", _can_pid, can_pid );

        sock_fd = socket(PF_CAN, SOCK_DGRAM, CAN_BCM);

        if (sock_fd < 0)
	{
                perror("Error abriendo socket");
                return -1;
	}

        addr.can_family = AF_CAN;

        strcpy( ifr.ifr_name, _can_interfaz );

        rc = ioctl(sock_fd, SIOCGIFINDEX, &ifr);

        if( rc < 0)
	{
                perror("Error configurando socket");
		close(sock_fd );
                return -2;
	}

        addr.can_ifindex = ifr.ifr_ifindex;

        rc = connect(sock_fd, (struct sockaddr *)&addr, sizeof(addr));

        if( rc < 0)
	{
                perror("Error conectando socket");
		close(sock_fd );
                return -3;
	}

	/* 
	 * Configuración del ciclo de transmisión
	 */
	msg.msg_head.opcode  = TX_SETUP;
	msg.msg_head.can_id  = can_id;
	msg.msg_head.flags   = SETTIMER | STARTTIMER | TX_CP_CAN_ID;
	msg.msg_head.nframes = 1;


	if( can_frecuencia == 0 )
		msg.msg_head.count   = 1;
	else
	{
		msg.msg_head.count   = 0;

		msg.msg_head.ival1.tv_sec = 0;
		msg.msg_head.ival1.tv_usec = 0;

		if( can_frecuencia == 1 )
		{
			msg.msg_head.ival2.tv_sec = 1;
			msg.msg_head.ival2.tv_usec = 0;
		}
		else
		{
			msg.msg_head.ival2.tv_sec = 0;
			msg.msg_head.ival2.tv_usec = 1000000/can_frecuencia;
		}
	}

	msg.frame[0].can_dlc   = 8;

	msg.frame[0].data[0]   = 0x02; /* 0x02 -> longitud 2 bytes */
	msg.frame[0].data[1]   = can_servicio; 
	msg.frame[0].data[2]   = can_pid; 
	msg.frame[0].data[3]   = 0x00;
	msg.frame[0].data[4]   = 0x00;
	msg.frame[0].data[5]   = 0x00;
	msg.frame[0].data[6]   = 0x00;
	msg.frame[0].data[7]   = 0x00;

    	rc = write( sock_fd, &msg, sizeof(msg) );

	if( rc != sizeof(msg) )
	{
                perror("Error escribiendo socket");
		close(sock_fd );
                return -4;
	}

	if( can_frecuencia != 0 )
	{
		printf("Para finalizar, pulsa una tecla: ");
    		getchar();
    		printf("\n");
	}

	close(sock_fd );
		
        return 0;
}
