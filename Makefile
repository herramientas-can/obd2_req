#
#    Copryright 2019 Miguel A. Pérez Valdenebro (miguelangel.perezvaldenebro@wilbord.com)
#
#    This file is part of obd2_req.
#
#    obd2_req is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    obd2_req is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with obd2_req.  If not, see <https://www.gnu.org/licenses/>.
#

TARGETS=$(DEP) obd2_req

CC=gcc
CCP=cpp

CFLAGS=-Wall -Wextra -pedantic

CFILES=obd2_req.c

OBJ=$(patsubst %.c,%.o,$(CFILES))
DEP=$(patsubst %.o,%.d,$(OBJ))

#include $(DEP)

%.d: %.c
	$(CPP) $(CFLAGS) $< -MM -MT $(@:.d=.o) >$@

%.o: %.c
	$(CC) -c $(CFLAGS) $< -o $@

obd2_req: $(OBJ)
	$(CC) $^ $(LIBS) -o $@

build: $(TARGETS)  \
	tags

all: build

tags: $(CFILES)
	ctags -R 

clean:
	rm -fr $(OBJ) $(TARGETS) tags
