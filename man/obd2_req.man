.\"
.\"    Copyright 2019 Miguel A. Pérez Valdenebro (miguelangel.perezvaldenebro@wilbord.com)
.\"
.\"    This file is part of obd2_req
.\"
.\"    obd2_req is free software: you can redistribute it and/or modify
.\"    it under the terms of the GNU General Public License as published by
.\"    the Free Software Foundation, either version 3 of the License, or
.\"    (at your option) any later version.
.\"
.\"    obd2_req is distributed in the hope that it will be useful,
.\"    but WITHOUT ANY WARRANTY; without even the implied warranty of
.\"    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\"    GNU General Public License for more details.
.\"
.\"    You should have received a copy of the GNU General Public License
.\"    along with obd2_req  If not, see <https://www.gnu.org/licenses/>.
.\"
.TH obd2_req 1 "27 Nov 2019" "1.0" "Página de manual de obd2_req"
.
.SH NOMBRE
obd2_req \- genera consultas periodicas a la centralita usando los servicios obdii
.
.SH SINOPSIS
.B obd2_req
[OPCIONES]
.
.SH DESCRIPCION
obd2_req permite programar una consulta por obdii a una cierta frecuencia a la que la centralita del vehículo responderá mediante la correspondiente trama OBDII para responder a dicha solicitud. Una vez programada la consulta, el programa generará las consultas periódicamente y se quedará a la espera de que se pulse una tecla para finalizar de enviar mensajes y terminar. Las respuestas a las consultas realizadas no son tratadas por el programa.
.SH OPCIONES
obd2_req soporta las siguientes opciones:
.TP
.BR "-i INTERFAZ CAN"
Especifica el interfaz CAN por el que se establece la comunicación. Por defecto, es decir, en caso de no usar esta opción se usa can0.
.TP
.BR "-f FRECUENCIA"
Especifica la frecuencia (en Hz) a la que se desean las solicitudes. Por defecto se usa 10 Hz.
.TP
.BR "-c CANID"
Especifica el identidicador CAN del mensaje a enviar (en hexadecimal). Por defecto se usa 0x7DF.
.TP
.BR "-s SERVICIO"
Especifica el servicio OBDII (en hexadecimal) a utilizar. Por defecto se usa 0x01.
.TP
.BR "-p PID"
Especifica el PID (en hexadecimal) deñ servicio OBDII a utilizar. Por defecto se usa 0x03.
.SH EJEMPLOS
.TP
.BR "obd2_req -i can2 -f 40 -p E"
Solicita por el interfaz can2 el avance de encendido (PID 0x0E), del servicio OBDII 0x01 (por defecto) para el CANID 0x7DF (por defecto, que es la dirección de difusion general para OBDII) con una frecuencia de 40 Hz.
.SH VER TAMBIÉN
odb2_modes(1)
.SH BUGS
No se conocen bugs.
.SH AUTOR
Miguel Angel Pérez Valdenebro (miguelangel.perezvaldenebro@wilbord.com)

